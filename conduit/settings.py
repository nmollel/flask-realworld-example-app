import os
from datetime import timedelta
from typing import (
    Any,
    Callable,
)


def _coerce_env_var(name: str, fn: Callable, default: Any) -> Any:
    value = os.environ.get(name)
    if value:
        try:
            return fn(value)
        except Exception:
            pass
    return default


PRODUCTION = "production"
API_ROOT_PATH = "/api"
FLASK_ENV = os.environ.get("FLASK_ENV", PRODUCTION)
# database
POSTGRES_HOST = os.environ.get("POSTGRES_HOST")
POSTGRES_DB = os.environ.get("POSTGRES_DB")
POSTGRES_USER = os.environ.get("POSTGRES_USER")
POSTGRES_PASSWORD = os.environ.get("POSTGRES_PASSWORD")

JWT_SECRET_KEY = os.environ.get("JWT_SECRET_KEY")
JWT_ACCESS_TOKEN_EXPIRES = os.environ.get("JWT_ACCESS_TOKEN_EXPIRES")
assert all(
    {POSTGRES_HOST, POSTGRES_DB, POSTGRES_USER, POSTGRES_PASSWORD, JWT_SECRET_KEY},
)

CONFIG_MAPPING = {
    "SQLALCHEMY_DATABASE_URI": f"postgresql://{POSTGRES_USER}:{POSTGRES_PASSWORD}@{POSTGRES_HOST}/{POSTGRES_DB}",
    "SQLALCHEMY_TRACK_MODIFICATIONS": False,
    "JWT_SECRET_KEY": JWT_SECRET_KEY,
    "JWT_ACCESS_TOKEN_EXPIRES": _coerce_env_var(
        "JWT_ACCESS_TOKEN_EXPIRES",
        lambda v: timedelta(minutes=int(v)),
        timedelta(minutes=15) if FLASK_ENV == PRODUCTION else timedelta(minutes=60),
    ),
    "JWT_REFRESH_TOKEN_EXPIRES": _coerce_env_var(
        "JWT_REFRESH_TOKEN_EXPIRES",
        lambda v: timedelta(hours=int(v)),
        timedelta(hours=24),
    ),
    "JWT_HEADER_TYPE": "Token",
}
