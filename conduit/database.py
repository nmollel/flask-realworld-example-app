from datetime import datetime

from conduit.extensions import db


class PKMixin(db.Model):
    __abstract__ = True
    id = db.Column(db.BigInteger, primary_key=True)


class TimestampMixin(db.Model):
    __abstract__ = True
    datetime_created = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    datetime_updated = db.Column(
        db.DateTime,
        nullable=False,
        default=datetime.utcnow,
        onupdate=datetime.utcnow,
    )
