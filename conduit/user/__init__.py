from typing import Optional

from flask import (
    Blueprint,
    abort,
)
from flask_apispec.annotations import (
    marshal_with,
    use_kwargs,
)
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
)
from flask_jwt_extended import get_current_user as flask_get_current_user
from flask_jwt_extended import jwt_required

from conduit.settings import API_ROOT_PATH

from .models import User
from .serializers import user_schema

BLUEPRINT_NAME = "user"
blueprint = Blueprint(BLUEPRINT_NAME, __name__)


def _generate_tokens(user: User) -> User:
    user.token = create_access_token(identity=user, fresh=True)
    user.refresh_token = create_refresh_token(identity=user)

    return user


@blueprint.route(f"{API_ROOT_PATH}/users", methods=["POST"])
@use_kwargs(user_schema)
@marshal_with(user_schema)
def register_user(username: str, email: str, password: str, **kwargs) -> Optional[User]:
    try:
        user = User.create(username=username, email=email, password=password)
        return _generate_tokens(user)
    except Exception:
        abort(400)


@blueprint.route(f"{API_ROOT_PATH}/users/login", methods=["POST"])
@use_kwargs(user_schema)
@marshal_with(user_schema)
def login(email: str, password: str, **kwargs) -> Optional[User]:
    user = User.get_user_by(email=email)
    if user and user.verify_password(password):
        return _generate_tokens(user)
    else:
        abort(400)


@blueprint.route(f"{API_ROOT_PATH}/user", methods=["GET"])
@jwt_required()
@marshal_with(user_schema)
def get_current_user() -> User:
    return flask_get_current_user()


@blueprint.route(f"{API_ROOT_PATH}/user", methods=["PUT"])
@jwt_required()
@use_kwargs(user_schema)
@marshal_with(user_schema)
def update_user(**kwargs) -> User:
    if kwargs:
        user: User = flask_get_current_user()
        user.update(**kwargs)
        return user
    else:
        abort(400)
