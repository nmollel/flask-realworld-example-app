from marshmallow import (
    Schema,
    fields,
    post_dump,
    pre_load,
)


class UserSchema(Schema):
    email = fields.Email()
    token = fields.Str(dump_only=True)
    refresh_token = fields.Str(dump_only=True)
    username = fields.Str()
    bio = fields.Str()
    image = fields.Url()
    password = fields.Str(load_only=True)

    @pre_load
    def load_user(self, data, **kwargs):
        if "user" in data:
            return data["user"]
        else:
            return data

    @post_dump
    def dump_user(self, data, **kwargs):
        return {"user": data}


user_schema = UserSchema()
user_schemas = UserSchema(many=True)
