from datetime import datetime
from enum import Enum
from typing import (
    Mapping,
    Optional,
)

from argon2 import PasswordHasher
from argon2.exceptions import (
    InvalidHash,
    VerificationError,
    VerifyMismatchError,
)

from conduit.database import (
    PKMixin,
    TimestampMixin,
)
from conduit.extensions import db


class AccountStatus(Enum):
    Inactive = 0
    Active = 1
    Suspended = 3


ph = PasswordHasher()


class User(PKMixin, TimestampMixin, db.Model):
    __tablename__ = "users"
    email = db.Column(db.String(255), nullable=False, unique=True)
    verified_email = db.Column(db.Boolean, default=False)
    username = db.Column(db.String(128), nullable=False, unique=True)
    bio = db.Column(db.String(280), nullable=True)
    password = db.Column(db.LargeBinary, nullable=False)
    image = db.Column(db.String(140), nullable=True)
    status = db.Column(
        db.Enum(AccountStatus),
        nullable=False,
        server_default=AccountStatus.Active.name,
    )
    last_login = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    token: str = ""
    refresh_token: str = ""

    def __init__(
        self, email: str, username: str, password: str, **kwargs: Mapping[str, str]
    ) -> None:
        db.Model.__init__(self, username=username, email=email, **kwargs)
        self.set_password(password)

    def set_password(self, password: str):
        self.password = ph.hash(password).encode("utf-8")

    def verify_password(self, password: str) -> bool:
        # TODO: add metrics to track the logic below
        try:
            return ph.verify(self.password, password)
        except (VerifyMismatchError, VerificationError):
            return False
        except InvalidHash:
            return False
        except Exception:
            return False

    def update(self, **kwargs):
        if "password" in kwargs:
            password = kwargs.pop("password")
            self.set_password(password)
        return super().update(**kwargs)

    @classmethod
    def get_user_by(cls, **kwargs) -> Optional["User"]:
        return cls.query.filter_by(**kwargs).one_or_none()

    def __repr__(self) -> str:
        return f"<User({self.id},{self.username!r})>"
