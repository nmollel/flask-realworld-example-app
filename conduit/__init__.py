from typing import Tuple

from flask import (
    Flask,
    jsonify,
)
from flask.wrappers import Response
from werkzeug.exceptions import HTTPException

import conduit.utils  # noqa
from conduit import user
from conduit.extensions import (
    db,
    jwt_manager,
    migrate,
)
from conduit.settings import CONFIG_MAPPING


def _extentions(app: Flask):
    db.init_app(app)
    migrate.init_app(app, db)
    jwt_manager.init_app(app)


def _blueprints(app: Flask):
    app.register_blueprint(user.blueprint)


def json_error_handler(e: Exception) -> Tuple[Response, int]:
    if isinstance(e, HTTPException) and e.code:
        return jsonify(error=str(e)), e.code
    else:
        return jsonify(error=str(e)), 500


def create_app() -> Flask:
    app = Flask(__name__, static_folder=None)
    app.config.from_mapping(CONFIG_MAPPING)

    _extentions(app)
    _blueprints(app)

    for code in [400, 404, 500]:
        app.register_error_handler(code, json_error_handler)

    return app
