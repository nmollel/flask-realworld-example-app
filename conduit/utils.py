from typing import (
    Mapping,
    Optional,
)

from conduit.extensions import jwt_manager
from conduit.user.models import User


@jwt_manager.user_identity_loader
def user_identity_lookup(user: User) -> int:
    return user.id


@jwt_manager.user_lookup_loader
def user_lookup_callback(_jwt_header: Mapping, jwt_data: Mapping) -> Optional[User]:
    return User.get_user_by(id=jwt_data["sub"])
