## Tasks
- spec
    + [get current user](https://realworld-docs.netlify.app/docs/specs/backend-specs/endpoints#get-current-user)
    + [update user](https://realworld-docs.netlify.app/docs/specs/backend-specs/endpoints#update-user)
- add user registration validation
    + password length and character combinations
- forgot password logic
- configure opentelemetry
- collect excepted exceptions metrics
