## Goals
At a high level, I want to implement a realworld app backend. While doing this, I would also want to spend time and consider aspects of running and maintaining the app in production. In that respect, I will consider security aspects, metrics and load testing.
- [ ] implement [realworld](https://github.com/gothinkster/realworld) backend api
- [ ] follow [twelve factor app](https://12factor.net/) principles
- [ ] setup ci checks for tests and code quality
- [ ] add opentelemetry instrumentation
- [ ] configure loadtest with [tsung](http://tsung.erlang-projects.org/user_manual/)

    `tsung` choice here is intentional because I am an `erlang` fan and it appears to have low resource requirements to generate significant traffic.
- [ ]
## Development
### Dependencies
1. [flask](https://flask.palletsprojects.com/en/2.0.x/)
2. [gunicorn](https://gunicorn.org/)
3. [SQLAlchemy](https://www.sqlalchemy.org/) extensions for `flask` namely
    - [flask-sqlalchemy](https://flask-sqlalchemy.palletsprojects.com/en/2.x/)
    - [flask-migrate](https://flask-migrate.readthedocs.io/en/latest/index.html)
    - [flask-apispec](https://flask-apispec.readthedocs.io/en/latest/index.html)
    - [flask-jwt-extended](https://flask-jwt-extended.readthedocs.io/en/stable/)
4. chaching
5. tracing
6. async tasks
    - track account modification events
### Models
- user
    ```
    id
    email
    email_verified
    username
    bio [280 chars, tweet length]
    image
    password
    account_status
    last_login
    date_created
    date_updated
    ```
### Useful Commands
- connect to database
    ```shell
    export PGHOST=$POSTGRES_HOST; \
    export PGUSER=$POSTGRES_USER; \
    export PGPASSWORD=$POSTGRES_PASSWORD; \
    export PGDATABASE=$POSTGRES_DB; \
    pgcli
    ```
- user test
    ```python
    from conduit.user.models import User
    User.create(email='email@gmail.com',username='test_user',password='thesecretpass')
    ```
